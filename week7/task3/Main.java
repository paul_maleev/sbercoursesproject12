package week7.task3;

public class Main {

    public static void main(String[] args) {
        Robot robot = new Robot();

        robot.go();
        robot.go();
        robot.go();
        robot.go();
        robot.turnLeft();
        robot.go();
        robot.go();
        robot.go();
        robot.turnLeft();
        robot.turnLeft();
        robot.go();
        robot.go();
        robot.go();
        robot.turnRight();
        robot.go();
    }
}
