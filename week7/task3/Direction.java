package week7.task3;

//0 -- up, 1 -- right, 2 -- bottom, 3 -- left
public enum Direction {
    UP(0),
    RIGHT(1),
    BOTTOM(2),
    LEFT(3);

    public final int number;

    Direction(int number) {
        this.number = number;
    }

    public static Direction ofNumber(int number) {
        for (Direction direction : values()) {
            if (direction.number == number) {
                return direction;
            }
        }
        return null;
    }
}
