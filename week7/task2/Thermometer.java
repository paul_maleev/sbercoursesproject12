package week7.task2;

/*

Реализовать класс “Термометр”.
Необходимо иметь возможность создавать инстанс класса с текущей температурой и
получать значение в фаренгейте и в цельсии.
 */

public class Thermometer {

    private double tempCelsius;
    private double tempFahrenheit;

    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }

    public Thermometer(double currentTemperature, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == TemperatureUnit.CELSIUS) {
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        } else if (temperatureUnit == TemperatureUnit.FAHRENHEIT){
            tempFahrenheit = currentTemperature;
            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
        } else {
            System.out.println("Не удалось распознать ед. измерения, будет установлен цельсий");
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        }
    }

    private double fromCelsiusToFahrenheit(double currentTemperature) {
        return currentTemperature * 1.8 + 32;
    }

    private double fromFahrenheitToCelsius(double currentTemperature) {
        return (currentTemperature - 32) / 1.8;
    }
}
