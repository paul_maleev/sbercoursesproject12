package week7.task1;

/*
Хранить, включать и выключать люстру
 */
public class Chandelier {

    private Bulb[] chandelier;

    //добавить валидации
    public Chandelier(int countOfBulbs) {
        chandelier = new Bulb[countOfBulbs];

        for (int i = 0; i < countOfBulbs; i++) {
            chandelier[i] = new Bulb();
        }
    }

    public void turnOn() {
        for (Bulb bulb : chandelier) {
            bulb.turnOn();
        }
    }

    public void turnOff() {
        for (Bulb bulb : chandelier) {
            bulb.turnOff();
        }
    }

    //придумать бизнес логику
    public boolean isShining() {
        return chandelier[0].isShining();
    }
}
