package week4;

import java.util.Scanner;

/*
Перевести из двоичной системы счисления в десятичную
На вход подается строка с числом в двоичной системе счисления (длина не более 5 символов).
Необходимо вывести это число в десятичной системе счисления.

0 -> 0 -> 0 * 2^0 = 0 * 1 = 0
1 -> 1 -> 1 * 2^0 = 1 * 1 = 1
10 -> 2 -> 0 * 2^0 + 1 * 2^1 = 2
11 -> 3
100 -> 4 -> 1 * 2^2 = 4
101 -> 5
110 -> 6
111 -> 7
1000 -> 8

101 -> 1 * 2^0 + 0 * 2^1 + 1 * 2^2 = 1 + 0 + 4 = 5

 */

public class Task9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int decimalNumber = 0;
        int i = 0;

        //101 -> 1 * 2^0 + 0 * 2^1 + 1 * 2^2 = 1 + 0 + 4 = 5
        while (n != 0) {
            int r = n % 10;
            n = n / 10;

            decimalNumber += r * (int)Math.pow(2, i);
            i++;
        }

        System.out.println(decimalNumber);

    }
}
