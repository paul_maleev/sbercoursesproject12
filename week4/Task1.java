package week4;

import java.util.Scanner;

/*
Дано число n < 13, n > 0. Найти факториал числа n (n! = 1 * 2 * 3 * … * (n - 1) * n)
7 -> 5040
 */

public class Task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int res = 1;
        for (int i = 2; i <= n; i++) {
            res *= i; //res = res * i
            System.out.println("Промежуточный результат: " + res);
        }

        System.out.println("Итоговый результат: " + res);
    }
}
