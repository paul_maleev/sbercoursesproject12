package week4;

import java.util.Scanner;

/*
Запросить у пользователя число строго больше 0.
Повторять ввод до тех пор, пока не будет введено корректное число.
Вывести “Отлично”, когда ввели корректное число.
 */

public class Task8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите число n: ");
//        int n = scanner.nextInt();

//        while (n <= 0) {
//            System.out.println("Введите число n: ");
//            n = scanner.nextInt();
//        }

        int n;
        do {
            System.out.println("Введите число n: ");
            n = scanner.nextInt();
        } while (n <= 0);

        System.out.println("Отлично");
    }
}
