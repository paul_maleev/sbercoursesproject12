package week4;

public class ExampleWithPostPreInc {

    public static void main(String[] args) {

/* post-inc */
//        int i = 1;
//        int a = i++;
//
//        //int temp = i;
//        //i = i + 1;
//        //a = temp;
//
//        System.out.println(i); //2
//        System.out.println(a); //1


/* extra case */
//        int i = 1;
//        i = i++;
//        System.out.println(i);
//        //int temp = i;
//        //i = i + 1;
//        //i = temp;



/* pre-inc */
//        int i = 1;
//        int a = ++i;
//
//        //i = i + 1;
//        //a = i;
//
//        System.out.println(i); //2
//        System.out.println(a); //2

/*doesn't matter for cycles*/
//        int i = 1;
//        i++; //++i //i = i + 1;

    }
}
