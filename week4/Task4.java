package week4;

import java.util.Scanner;

/*
Начальный вклад в банке равен 1000.
Каждый месяц размер вклада увеличивается на P процентов от имеющейся суммы (0 < P < 25).
Найти через какое количество времени размер вклада будет больше 1100
и вывести  найденное количество месяцев и итоговой размер вклада.

15 ->
1
1150.0

3 ->
4
1125.50881

 */

public class Task4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();

        double start = 1000;
        double limit = 1100;
        int count = 0;

        while (start < limit) {
            start += start * p / 100;
            count++;
        }

        System.out.println(count);
        System.out.println(start);
    }
}
