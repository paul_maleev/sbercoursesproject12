package week3;

import java.util.Scanner;

/*
На вход подается две строки: первая содержит номер карты, вторая — пинкод.
Проверить, что первая состоит из 16 цифр, разделенных пробелом (вид XXXX XXXX XXXX XXXX, где X - цифра) и проверить,
что вторая состоит из 4 цифр.

Вывести true, если все ок, иначе false.

Пример:
12345678 n345 3994
12345
--> false

1234 5678 9123 2322
1234
--> true
 */

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        String pin = scanner.nextLine();

        String myRegex = "([0-9]{4} ){3}([0-9]{4})";
        if (number.matches(myRegex) && pin.matches("(\\d{4})")) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
