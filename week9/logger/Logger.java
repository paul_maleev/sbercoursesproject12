package week9.logger;

public interface Logger {

    void log(String message);
}
