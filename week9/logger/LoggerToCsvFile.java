package week9.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class LoggerToCsvFile extends FileNameHandler implements Logger {

    private final String extension = ".csv";

    @Override
    public void log(String message) {
        Writer writer;
        try {
            writer = new FileWriter(getDefaultFileName() + extension, true);
            writer.write(message + "\n");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getExtension() {
        return ".csv";
    }
}
