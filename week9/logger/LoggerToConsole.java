package week9.logger;

public class LoggerToConsole implements Logger {
    @Override
    public void log(String message) {
        System.out.println(message);
    }
}
