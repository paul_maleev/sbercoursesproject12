package week9.logger;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
 //       LoggerToConsole loggerToConsole = new LoggerToConsole();
        //loggerToConsole.log("Мы в main");

 //       LoggerToTxtFile loggerToTxtFile = new LoggerToTxtFile();
//        loggerToTxtFile.log("Мы в main");
//        loggerToTxtFile.log("Мы в main еще раз");

        Logger logger = new LoggerToCsvFile();
        Bulb bulb = new Bulb(logger);
        bulb.turnOn();
        bulb.isShining();
        bulb.turnOff();

        //LoggerEvaluator.checkLogger(logger);
    }
}
