package week9.logger;

public class LoggerEvaluator {

    public static void checkLogger(Logger logger) {
        if (logger instanceof LoggerToConsole) {
            System.out.println("Не очень хороший логгер");
        } else if (logger instanceof LoggerToTxtFile) {
            System.out.println("Прекрасный логгер");
        } else {
            System.out.println("Не знаю этот логгер");
        }
    }

}
