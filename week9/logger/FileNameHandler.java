package week9.logger;

public abstract class FileNameHandler {

    private String defaultFileName = "output2";

    public String getDefaultFileName() {
        return defaultFileName;
    }

    public abstract String getExtension();
}
