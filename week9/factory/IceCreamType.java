package week9.factory;

public enum IceCreamType {
    CHOCOLATE,
    VANILLA,
    CHERRY
}
