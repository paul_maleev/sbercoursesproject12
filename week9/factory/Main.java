package week9.factory;

public class Main {

    public static void main(String[] args) {
        IceCreamFactory factory = new IceCreamFactory();

        IceCream iceCream = factory.getIceCream(IceCreamType.CHERRY);
        if (iceCream != null) {
            iceCream.printIngredients();
        }

    }
}
