package week6;

import java.util.Scanner;

/*
На вход подается число N — ширина и высота матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.

Необходимо изменить её так, чтобы она стала симметричной относительно главной диагонали
(отобразить элементы, расположенные над главной диагональю вниз). Главную диагональ заполнить нулями.

Главной диагональю называется диагональ, проходящая из верхнего левого угла в правый нижний.

3
1 2 3
4 5 6
7 8 9
->
0 2 3
2 0 6
3 6 0

 */

public class Task9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (i == j) {
                    a[i][j] = 0;
                } else {
                    a[j][i] = a[i][j];
                }
            }
        }

        printArray(n, a);
    }

    private static void printArray(int n, int[][] a) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
