package week6;

import java.util.Scanner;

/*
Треугольник Паскаля.
На вход подается натуральное N. Необходимо вывести матрицу, заполненную по следующему правилу:
a[i][0] = 1
a[j][0] = 1
a[i][j] = a[i - 1][j] + a[i][j - 1]

3
->
1 1 1
1 2 3
1 3 6

5
->
1 1 1 1 1
1 2 3 4 5
1 3 6 10 15
1 4 10 20 35
1 5 15 35 70

 */

public class Task8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] pascal = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) {
                    pascal[i][j] = 1;
                } else {
                    pascal[i][j] = pascal[i - 1][j] + pascal[i][j - 1];
                }
            }
        }

        printArray(n, pascal);

    }

    private static void printArray(int n, int[][] a) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
