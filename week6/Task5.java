package week6;

import java.util.Scanner;

/*
На вход подается число N — ширина и высота матрицы.
Необходимо заполнить матрицу 1 и 0 в виде шахматной доски. Нулевой элемент должен быть 0.

3
->
0 1 0
1 0 1
0 1 0

 */

public class Task5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] a = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = (i + j) % 2 == 0 ? 0 : 1;
            }
        }

//        int sum = 0;
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < n; j++) {
//                a[i][j] = (sum) % 2 == 0 ? 0 : 1;
//                sum++;
//            }
//            if (n % 2 == 0) {
//                sum++;
//            }
//        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
