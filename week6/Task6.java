package week6;

/*
На вход подается число N — ширина и высота матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
После этого передается натуральное число P.

Необходимо найти в матрице число P и занулить строку и столбец,
в котором это число находится (кроме числа P).

Применить эту операцию ко всем найденным числам P.

3
1 2 3
1 5 5
1 2 3
5
->
1 0 3
0 5 0
1 0 3

7
1 2 3 4 5 6 7
8 9 8 9 6 5 4
3 2 1 2 3 4 5
6 7 8 9 8 7 6
6 5 4 3 2 1 2
3 4 5 6 7 8 9
8 7 6 5 4 3 2
9
->
1 0 3 0 5 6 0
0 9 0 0 0 0 0
3 0 1 0 3 4 0
0 0 0 9 0 0 0
6 0 4 0 2 1 0
0 0 0 0 0 0 9
8 0 6 0 4 3 0
 */

import java.util.Scanner;

public class Task6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] == p) {
                    for (int k = 0; k < n; k++) {
                        if (a[k][j] != p) {
                            a[k][j] = 0;
                        }
                        if (a[i][k] != p) {
                            a[i][k] = 0;
                        }
                    }
                }
            }
        }



        printArray(n, a);
    }

    private static void printArray(int n, int[][] a) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
