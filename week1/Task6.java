package week1;

import java.util.Scanner;

/*
Вычислить доход работника за неделю.
Прочитать из консоли данные количество рабочих часов в неделю, часовая ставка, налог.
Параметры
c - количество рабочих часов в неделю
r - ставка за час
t - налог
 */


public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();
        int r = scanner.nextInt();
        int t = scanner.nextInt();

        double result = c * r * (100. - t) / 100;

        System.out.println(result);
    }
}
