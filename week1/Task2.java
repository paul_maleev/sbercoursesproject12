package week1;

import java.util.Scanner;

/*
Дано m – количество гигабайт трафика, используемое пользователем за месяц, с – заплаченная цена за этот трафик.
Вычислить стоимость одного гигабайта трафика.
Ограничения:
0 < c < 100
 */

//m/c = 1/x -> mx = c -> x = c / m

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(c * 1. / m);
    }
}
