package week1;

import java.util.Scanner;

/*
n - число детей
k - число конфет

1. Вывести сколько конфет достанется каждому ребенку, если стараться делить их поровну
2. Вывести сколько конфет останется после выдачи конфет детям
 */


public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int k = scanner.nextInt();

        System.out.println(k / n);
        System.out.println(k % n);
    }
}
