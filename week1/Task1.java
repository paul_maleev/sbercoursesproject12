package week1;

import java.util.Scanner;

/*
Даны числа a, b, c. Перенести значения из a в b, из b в c, из c в a
 */

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();

        System.out.println("a = " + a + " b = " + b + " c = " + c);

        int temp = c;
        c = b;
        b = a;
        a = temp;

        System.out.println("a = " + a + " b = " + b + " c = " + c);
    }
}


