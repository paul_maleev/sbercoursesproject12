package week1;

import java.util.Scanner;

/*
Решение квадратного уравнения.
Даны целые числа a, b и с, определяющие квадратное уравнение.
Вычислить корни уравнения.
Гарантируется, что в тестовых данных у всех уравнений есть решение и их два.
 */

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        double d = Math.pow(b, 2) - 4 * a * c;
        double x1 = (-b + Math.sqrt(d)) / (2 * a);
        double x2 = (-b - Math.sqrt(d)) / (2 * a);

        System.out.println(x1);
        System.out.println(x2);
    }
}
