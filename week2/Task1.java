package week2;

import java.util.Scanner;

/*
Дано число n. Нужно проверить четное ли оно.
 */

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        String str;
//        if (n % 2 == 0) {
//            str = "Число четное";
//        } else {
//            str = "Число нечетное";
//        }

        String str = (n % 2 == 0) ? "Число четное" : "Число нечетное";
        System.out.println(str);

        System.out.println((n % 2 == 0) ? "Число четное" : "Число нечетное");

        //System.out.println(n % 2 == 0);


    }
}
