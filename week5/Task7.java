package week5;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив целых чисел из N элементов.
Необходимо циклически сдвинуть элементы на 1 влево.

5
1 2 3 4 7
<--
2 3 4 7 1
 */

public class Task7 {

    public static void main(String[] args) {
        int arr[] = inputArray();

        int temp = arr[0];
//        for (int i = 0; i < arr.length - 1; i++) {
//            arr[i] = arr[i + 1];
//        }


        System.arraycopy(arr, 1, arr, 0, arr.length - 1);
        arr[arr.length - 1] = temp;

        printArray(arr);
    }



    public static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();
        }

        return arr1;
    }

    public static void printArray(int[] arr) {
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }
}
