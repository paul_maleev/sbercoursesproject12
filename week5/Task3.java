package week5;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив целых чисел длины N.

Проверить, является ли он отсортированным массивом по строго убыванию. Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false
 */

public class Task3 {

//    public static void outputSomeLines() {
//        for (int i = 0; i < 5; i++) {
//            for (int j = 0; j < 5; j++) {
//                if (j > 2) {
//                    return;
//                }
//                System.out.print(j + " ");
//            }
//            System.out.println();
//        }
//    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.println(checkIfArrayDesc(arr));
    }

    public static boolean checkIfArrayDesc(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] <= arr[i + 1]) {
                return false;
            }
        }
        return true;
    }
}
