package week5;

import java.util.Arrays;
import java.util.Scanner;

/*

На вход подается два отсортированных массива по возрастанию.
Нужно создать отсортированный третий, состоящий из элементов первых двух.

5
1 2 3 4 7
2
1 6

        i
1 2 3 4 7
4 6
  j

        i
1 2 3 4 5
0
j

result:
0 1 2 3 4 5
    k
result:
1 2 3 4 4 6 7

 */

public class Task5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int[] arr2 = new int[m];
        for (int i = 0; i < m; i++) {
            arr2[i] = scanner.nextInt();
        }

        int i = 0;
        int j = 0;
        int k = 0;

        int[] result = new int[arr1.length + arr2.length];
        while (i < arr1.length && j < arr2.length) {
            result[k++] = arr1[i] < arr2[j] ? arr1[i++] : arr2[j++];
        }

        while(i < arr1.length) {
            result[k++] = arr1[i++];
        }

        while(j < arr2.length) {
            result[k++] = arr2[j++];
        }

        for (int elem : result) {
            System.out.println(elem);
        }

        //mergeArraysWay1(arr1, arr2);
    }

    private static void mergeArraysWay1(int[] arr1,int[] arr2) {
        int[] result = new int[arr1.length + arr2.length];

        for (int i = 0; i < arr1.length; i++) {
            result[i] = arr1[i];
        }

        int k = arr1.length;
        for (int i = 0; i < arr2.length; i++) {
            result[k] = arr2[i];
            k++;
        }

        Arrays.sort(result);

        for (int i = 0; i < arr1.length + arr2.length; i++) {
            System.out.print(result[i] + " ");
        }
    }
}
