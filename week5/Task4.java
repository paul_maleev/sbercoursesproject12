package week5;

public class Task4 {

    public static void main(String[] args) {

        int a = 3;
        int b = 5;
        swap(a, b); //не изменит значения a и b, т.к. передача в метод по значению

        int[] arr = new int[3];
        foo1(arr); //изменения не произойдут, т.к. ссылка на массив передается в метод по значению
        System.out.println(arr[0]);

        foo2(arr); //изменения произойдут,
                   // т.к. через передачу ссылки по значению в метод можно обратиться
                    // к области памяти хранения элементов массива
        System.out.println(arr[0]);
    }

    public static void swap(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
    }

    public static void foo1(int[] arr) {
        arr = null; //arr = new int[5];
    }

    public static void foo2(int[] arr) {
        arr[0] = 777;
    }
}
